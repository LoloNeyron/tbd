
        //stats des joueurs

        const data = [{joueur : [
            //premier objet vide : j1 == joueur[1]
            {}, {pv: 100,pvMax: 100, mana:30, manaMax: 30}, {pv: 100,pvMax: 100, mana:30, manaMax: 30},{pv: 100,pvMax: 100, mana:30, manaMax: 30},{pv: 100,pvMax: 100, mana:30, manaMax: 30},
        ],
        combo : 0,
        comboIsActive : false,

        joueurMort : [],
        joueurQuiATaper : [],

        //stats du monstre
        Monster : { pv: 800, pvMax: 800, mana: 100, manaMax: 100},
        degatMonster : "",

        capacite1J1 : "", capacite2J1 : "",  capacite3J1 : "",  capacite4J1 : "", capacite1J2 : "", capacite2J2 : "", capacite3J2 : "", capacite4J2 : "",capacite1J3 : "",capacite2J3 : "",capacite3J3 : "",capacite4J3 : "",capacite1J4 : "",capacite2J4 : "",capacite3J4 : "",capacite4J4 : "",
        capacite1Monstre: "", capacite2Monstre: "", capacite3Monstre: "", capacite4Monstre: "",
//liste des capacité offensive
        attackSkill : [
             { 
                name : 'Poing',
                description: "",
                heal : 0,
                dmg : 1,
                mana : 1,
                gainMana : 0,
                buff : "",
                combo : 3
            },
             { 
                name : "Pied",
                description: "",
                heal : 0,
                dmg : 5,
                mana : 5,
                gainMana : 0,
                buff : "",
                combo : 2
            },
            { 
                name : "Dague",
                description: "",
                heal : 0,
                dmg : 7,
                mana : 10,
                gainMana : 0,
                buff : "",
                combo : 0
            },
            { 
                name : "Fire",
                description: "",
                heal : 0,
                dmg : 6,
                gainMana : 0,
                mana : 10,
                buff : "",
                combo : 2
            },
        ],
//liste des capacité deffensive
        defenseSkill : [
            {
                name : 'soin1',
                description: "",
                heal: 5,
                dmg: 0,  
                gainMana : 0,         
                mana : 3,
                buff : "",
                combo : 0
                                                                
            },
            {
                name : 'soin2',
                description: "",
                heal: 7,
                dmg: 0,  
                gainMana : 0,         
                mana : 5,
                buff : "",
                combo : 0
                                                                
            },
            {
                name : 'soin3',
                description: "",
                heal: 9,
                dmg: 0,    
                gainMana : 0,       
                mana : 10,
                buff : "",
                combo : 0
                                                                
            },
            {
                name : 'soin4',
                description: "",
                heal: 11,
                dmg: 0, 
                gainMana : 0,          
                mana : 3,
                buff : "",
                combo : 0
                                                                
            },
        ],
//liste des capacité régenerante
        manaSkill : [
            {
                name : 'regen',
                description: "",
                heal: 0,
                dmg: 0, 
                gainMana : 10,          
                mana : 0,
                buff : "",
                combo : 0
            },
            {
                name : 'regen 2',
                description: "",
                heal: 0,
                dmg: 0, 
                gainMana : 15,          
                mana : 0,
                buff : "",
                combo : 0
            },
            {
                name : 'regen 3',
                description: "",
                heal: -5,
                dmg: 0, 
                gainMana : 20,          
                mana : 0,
                buff : "",
                combo : 0
            },
            {
                name : 'regen 4',
                description: "",
                heal: -10,
                dmg: 0,
                gainMana : 25,
                mana : 0,
                buff : "",
                combo : 0
            },
        ],}]

        export default data;