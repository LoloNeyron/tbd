<script src="https://unpkg.com/vue@latest/dist/vue.js"></script>
<script>
var app = new Vue({
    el: '#app',

    data: {
        //stats des joueurs

        joueur : [
            //premier objet vide : j1 == joueur[1]
            {},
             {name: "",pv: 100,pvMax: 100, mana:30, manaMax: 30},
             {name: "",pv: 100,pvMax: 100, mana:30, manaMax: 30},
             {name: "",pv: 100,pvMax: 100, mana:30, manaMax: 30},
             {name: "",pv: 100,pvMax: 100, mana:30, manaMax: 30},
        ],
        combo : 0,
        comboIsActive : false,

        joueurMort : [],
        joueurVivant : [1,2,3,4],
        joueurQuiATaper : [],

        //stats du monstre
        Monster : { pv: 800, pvMax: 800, mana: 100, manaMax: 100},

        degatMonster : "",

        capacite1J1 : "", capacite2J1 : "",  capacite3J1 : "",  capacite4J1 : "", capacite1J2 : "", capacite2J2 : "", capacite3J2 : "", capacite4J2 : "",capacite1J3 : "",capacite2J3 : "",capacite3J3 : "",capacite4J3 : "",capacite1J4 : "",capacite2J4 : "",capacite3J4 : "",capacite4J4 : "",
        capaciteMonstre1: "", capaciteMonstre2: "", capaciteMonstre3: "", capaciteMonstre4: "",

//liste des capacité offensive
        attackSkill : <?php include($_SERVER["DOCUMENT_ROOT"].'/../resources/views/data.js'); ?>,
//liste des capacité deffensive
        defenseSkill : <?php include($_SERVER["DOCUMENT_ROOT"].'/../resources/views/defenseskillsData.js');?>,
//liste capacité du monstre
        monsterSkill: <?php include($_SERVER["DOCUMENT_ROOT"].'/../resources/views/monsterSkillData.js');?>,
//liste de nom pour personnage
        listOfSomeName: <?php include($_SERVER["DOCUMENT_ROOT"].'/../resources/views/listOfSomeName.js');?>,
//fin capacité
    },

    methods: {
        styleWidthProgessBar(pv, pvMax) {
            let pourcentage = (pv * 100) / pvMax;
            return "width:" + pourcentage + "%";
        },

//attaque du joueur 
        degat: function(capacite, i){
            console.time();
            
            //récupération du donneur de coup
            let clicked = document.getElementById("joueur" + i);
            
            //verification si le joueur est mort
            if(!this.joueurMort.includes(i)){
                if(this.Monster.pv > 0){
                
                    //verification si le joueur a jouer.
                    if(this.joueurQuiATaper.includes(i)){
                        this.messageNotifError('NOPE', 'ce perssonage a deja joué.', 'fa fa-times');
                    }else{
                        
                        //gestion des modificateur de status
                        if(((this.joueur[i].pv + capacite.heal) <= this.joueur[i].pvMax) &&((this.joueur[i].mana + capacite.gainMana) <= this.joueur[i].manaMax)){
                            
                            this.joueur[i].pv += capacite.heal;

                            if(capacite.heal > 0){
                                clicked.classList.add("AnimationHeal");
                                setTimeout(() => { 
                                    clicked.classList.remove('AnimationHeal');
                                }, 300)
                            }

                            this.joueur[i].mana += capacite.gainMana;
                            this.joueur[i].mana -= capacite.mana;
                            //décrémentation des pv du monstre
                            this.Monster.pv -= capacite.dmg;
                            //une chance sur 4 que le monstre contre 
                            if(capacite.dmg > 0){
                                setTimeout(() => { 
                                    this.monsterDegat(i, true);
                                }, 500);
                            }
                            //le donneur de coup a jouer
                            this.joueurQuiATaper.push(i);
                            clicked.classList.add('selected');
                            
                            }else if ((this.joueur[i].mana + capacite.gainMana) <= this.joueur[i].manaMax){
                                this.messageNotifError('Vie', 'Vous avez trop de points de vie.', 'fas fa-heartbeat');
                            }else if ((this.joueur[i].pv + capacite.heal) <= this.joueur[i].pvMax){
                                this.messageNotifError('Mana', 'Vous avez trop de mana.', 'fas fa-fire');
                        }
                        //animation des degats sur le monstre
                        
                        //trigger combo                                                                        
                        if(this.comboIsActive == false){
                            this.comboAtack(capacite, i);    
                        }     
                        
                        if(capacite.dmg > 0){
                           this.animationDegats(capacite)
                        }
                        
                    }

                    //réinitialisation des joueurs
                    if(this.joueurQuiATaper.length == (4 - (this.joueurMort.length))){
                            for(i = 0; i < this.joueurQuiATaper.length; i++){
                                document.getElementById("joueur" + this.joueurQuiATaper[i]).classList.remove('selected');
                            }
                            this.joueurQuiATaper = [];
                    //le monstre riposte au reset
                            setTimeout(() => { 
                                this.monsterDegat(i, false);
                            }, 500);
                        }
                }else {
                    //code si monstre mort
                    alert('il es mort !');
                }
            }else { 
                alert('joueur mort !')
            }
            console.timeEnd();
        },

//gestion des combos
        comboAtack: function(capacite, i){
            this.combo += capacite.combo;
                if(this.combo >= 8){
                    this.comboIsActive = true;
                    setTimeout(() => { 
                            this.comboIsActive = false;
                    }, 25000000 - (this.combo*10));
                }
        },
        comboDegat: function(){
            let comboCapacite = {dmg : 10};
            this.Monster.pv -= 10;
            this.animationDegats(comboCapacite);
            this.comboIsActive = false;
            this.combo -= 8;
        },

//gestion notification
        messageNotifError: function(title, message, icon){
            iziToast.error({
                title: title,
                message: message,
                timeout: 1300,
                position: 'bottomCenter',
                displayMode: "replace",
                theme: 'dark',
                icon: icon,
                backgroundColor: 'rgba(171, 56, 56, 1)',
                balloon: true,
                overlayColor: 'rgba(255, 0, 0, 0.1)',
                overlay: true,

            });
        },      

//animations
        animationDegats: function(capacite){
            let animationDesDegatSurMonstre = document.getElementById("monsterCard");
            let affichageDesDegatSurMonstre = document.getElementById("degatSpanMonster");
            
            animationDesDegatSurMonstre.classList.add('animationDegatsCard');
            affichageDesDegatSurMonstre.classList.add('animationAffichageDesDegats');

            affichageDesDegatSurMonstre.innerHTML = capacite.dmg;

            setTimeout(() => { 
                affichageDesDegatSurMonstre.classList.remove('animationAffichageDesDegats');
                animationDesDegatSurMonstre.classList.remove('animationDegatsCard');
                affichageDesDegatSurMonstre.innerHTML = "";
            }, 1000)
        },

//attaque du monstre
        monsterDegat: function(i, randomMode) {
            
            let attack = true; 

            if(randomMode == true){
                let chance = this.getRandomInt(3);
                
                if(chance !== 2){
                    attack = false
                }
            }
            
            if(attack == true){
                
                //ciblage avec part d'aléatoire
                let tapeQuelPerso = this.getRandomInt(3);
                        
                if(tapeQuelPerso == 0){
                    i = this.getRandomInt(3) + 1;
                }
                let QuelAttack = this.getRandomInt(4);
                //dégats du monstre
                this.degatMonster = this.capaciteMonstre[QuelAttack].dmg;
                //décrementation des pv du joueur
                this.joueur[i].pv -= this.degatMonster;
                //verification mort du joueur
                if(this.joueur[i].pv <= 0){
                    this.joueurMort.push(i);
                    //TODO suprimmer le joueur mort du tableaux joueurVivan[]
                    alert('J'+i+' est mort');
                }
                
                //animation des dégats
                let animationDesDegatsSurJoueur = document.getElementById("degatSpanJ" + i);
                let animationDesDegatsSurJoueurShake = document.getElementById("joueur" + i);
    
                animationDesDegatsSurJoueur.innerHTML = this.degatMonster;
                animationDesDegatsSurJoueur.classList.add('animationAffichageDesDegats');
                if(this.degatMonster !== 0){
                    animationDesDegatsSurJoueurShake.classList.add('animationDegatsCard');
                }else{
                    animationDesDegatsSurJoueur.innerHTML = "FAIL !";  
                }
                setTimeout(() => { 
                    animationDesDegatsSurJoueur.classList.remove('animationAffichageDesDegats');
                    animationDesDegatsSurJoueur.innerHTML = "";
                    animationDesDegatsSurJoueurShake.classList.remove('animationDegatsCard');
                }, 700);
            
            }
            
        },
        
//custom random integer
        getRandomInt: function(max) {
            return Math.floor(Math.random() * Math.floor(max));
        },

// initialise les capacités aléatoirement pour chaques joueurs
        init: function(){
            this.joueur[1].name = this.listOfSomeName[this.getRandomInt(this.listOfSomeName.length)].name;
            this.joueur[2].name = this.listOfSomeName[this.getRandomInt(this.listOfSomeName.length)].name;
            this.joueur[3].name = this.listOfSomeName[this.getRandomInt(this.listOfSomeName.length)].name;
            this.joueur[4].name = this.listOfSomeName[this.getRandomInt(this.listOfSomeName.length)].name;

            this.capacite1J1 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J1 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J1 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J1 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        
            this.capacite1J2 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J2 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J2 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J2 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        
            this.capacite1J3 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J3 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J3 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J3 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];
        
            this.capacite1J4 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite2J4 = this.attackSkill[this.getRandomInt(this.attackSkill.length)];
            this.capacite3J4 = this.manaSkill[this.getRandomInt(this.manaSkill.length)];
            this.capacite4J4 = this.defenseSkill[this.getRandomInt(this.defenseSkill.length)];

            this.capaciteMonstre = [
                                    this.monsterSkill[this.getRandomInt(this.monsterSkill.length)],
                                    this.monsterSkill[this.getRandomInt(this.monsterSkill.length)],
                                    this.monsterSkill[this.getRandomInt(this.monsterSkill.length)],
                                    this.monsterSkill[this.getRandomInt(this.monsterSkill.length)]
                                    ];

        },

//combo loss toutes les 5 sec
        startInterval : function() {
            setInterval(() => {
                this.combo -= 1;
                if(this.combo < 1){
                   this.combo = 0;
                }
                if(this.combo < 8){
                    this.comboIsActive = false
                }
            }, 5000)   
        },
        
    },
//init values
    beforeMount(){
        this.init();
        this.startInterval();
    },

})
</script>