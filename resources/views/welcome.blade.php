@extends('layout.app')
@section('content')
<div id="app">
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">

                </div>

                <div class="card-monstre col-sm-6">
                    <div id="monsterCard">
                        <div class="text-center">
                            <div class="row">
                                <div class="col-sm-2 offset-sm-3">
                                    <span class="badge badge-danger ml-2 " id="degatSpanMonster"></span>
                                    <img class="img-fluid" src="http://res.publicdomainfiles.com/pdf_view/67/13925387417373.png">
                                </div>
                                
                                    <div id="comboOnMonster" class="col-sm-4">
                                        <button 
                                            type="button" 
                                            class="btn btn-warning material-tooltip-main" 
                                            >
                                                @{{capaciteMonstre.name}}  
                                                <i class="fas fa-bomb"></i> @{{capaciteMonstre1.dmg}} 
                                                <i class="fas fa-fire-alt"></i> - @{{capaciteMonstre1.mana}}
                                        </button>
                                        <button 
                                            type="button" 
                                            class="btn btn-warning material-tooltip-main" 
                                            >
                                                @{{capaciteMonstre2.name}}  
                                                <i class="fas fa-bomb"></i> @{{capaciteMonstre2.dmg}} 
                                                <i class="fas fa-fire-alt"></i> - @{{capaciteMonstre2.mana}}
                                        </button>
                                        <button 
                                            type="button" 
                                            class="btn btn-warning material-tooltip-main" 
                                            >
                                                @{{capaciteMonstre3.name}}  
                                                <i class="fas fa-bomb"></i> @{{capaciteMonstre3.dmg}} 
                                                <i class="fas fa-fire-alt"></i> - @{{capaciteMonstre3.mana}}
                                        </button>
                                        <button 
                                            type="button" 
                                            class="btn btn-warning material-tooltip-main" 
                                            >
                                                @{{capaciteMonstre4.name}}  
                                                <i class="fas fa-bomb"></i> @{{capaciteMonstre4.dmg}} 
                                                <i class="fas fa-fire-alt"></i> - @{{capaciteMonstre4.mana}}
                                        </button>

                                    </div>
                                </div>    
                            </div>

                            <div class="progress bar-taille md-progress">
                                <div class="progress-bar progress-bar bg-warning" role="progressbar" :aria-valuenow="75.00" aria-valuemin="0" aria-valuemax="25" :style="styleWidthProgessBar( combo, 25)" role="progressbar" >COMBO @{{combo}}</div>   
                            </div>
                            <div class="progress bar-taille md-progress">
                                <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar( Monster.pv, Monster.pvMax)" :aria-valuenow="Monster.pv" aria-valuemin="0" :aria-valuemax="Monster.pvMax">
                                    <i class="fas fa-heart icon-text">  @{{ Monster.pv }}</i>
                                </div>   
                            </div>
                            <div class="progress bar-taille md-progress">
                                <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(Monster.mana, Monster.manaMax)" :aria-valuenow="Monster.mana" aria-valuemin="0" :aria-valuemax="Monster.manaMax">
                                    <i class="fas fa-fire-alt icon-text"> : @{{ Monster.mana }}</i>
                                </div>   
                            </div>
                        </div>
                    </div>    
                </div>

                <div class="col-sm-3">

                </div>
            </div>
    </section>


    <section class="container-fluid">
        <div class="row">
            <div class="col-sm-3 card center" id="joueur1">
                <div class="card-body text-center">
                <h5 class="card-title">@{{ joueur[1].name }}</h5>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[1].pv, joueur[1].pvMax)" :aria-valuenow="joueur[1].pv" aria-valuemin="0" :aria-valuemax="joueur[1].pvMax">
                            <i class="fas fa-heart icon-text"> : @{{joueur[1].pv}}</i>
                        </div>   
                    </div>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[1].mana, joueur[1].manaMax)" :aria-valuenow="joueur[1].mana" aria-valuemin="0" :aria-valuemax="joueur[1].manaMax">
                            <i class="fas fa-fire-alt icon-text"> : @{{joueur[1].mana}}</i>
                        </div>   
                    </div>
                    <span class="badge badge-danger ml-2 " id="degatSpanJ1"></span>
                        <div class="row">

                            <button v-if="capacite1J1.mana <= joueur[1].mana" 
                                    type="button" 
                                    v-on:click="degat(capacite1J1, 1)" 
                                    class="btn btn-success material-tooltip-main col-5" 
                                    >
                                        @{{capacite1J1.name}}  
                                        <i class="fas fa-bomb"></i> @{{capacite1J1.dmg}} 
                                        <i class="fas fa-fire-alt"></i> - @{{capacite1J1.mana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary col-5"
                                >
                                    @{{capacite1J1.name}}
                                    <i class="fas fa-fire-alt col-5"></i> - @{{capacite1J1.mana}}
                            </button>

                            <button 
                                v-if="capacite2J1.mana <= joueur[1].mana" 
                                type="button"
                                v-on:click="degat(capacite2J1, 1)" 
                                class="btn btn-success col-5"
                                >
                                    @{{capacite2J1.name}} <i class="fas fa-bomb"></i> @{{capacite2J1.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J1.mana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary col-5"
                                >
                                    @{{capacite2J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite2J1.mana}}
                            </button>

                            <button 
                                v-if="capacite3J1.mana <= joueur[1].mana" 
                                type="button"
                                v-on:click="degat(capacite3J1, 1)" 
                                class="btn btn-success col-5"
                                >
                                    @{{capacite3J1.name}} <i class="fas fa-heart"></i> @{{capacite3J1.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J1.gainMana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary col-5"
                                >
                                    @{{capacite3J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite3J1.mana}}
                            </button>

                            <button 
                                v-if="capacite4J1.mana <= joueur[1].mana" 
                                type="button"
                                v-on:click="degat(capacite4J1, 1)" 
                                class="btn btn-success col-5"
                                >
                                    @{{capacite4J1.name}} <i class="fas fa-heart"></i> @{{capacite4J1.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J1.mana}}
                            </button>

                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary "
                                >
                                    @{{capacite4J1.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite4J1.mana}}
                            </button>
                        </div>
                    </div>
                </div>

                <div id="middleCase" class="col-sm-6">
                <!-- Boutton COMBO pop alea -->
                <button v-if="comboIsActive" v-on:click="comboDegat" class="btn-combo btn btn-warning pulse">COMBO</button>
                </div>

                <div class="col-sm-3 card center" id="joueur4">
                    <div class="card-body text-center">
                        <h5 class="card-title">@{{ joueur[4].name }}</h5>
                        <div class="progress md-progress">
                            <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[4].pv, joueur[4].pvMax)" :aria-valuenow="joueur[4].pv" aria-valuemin="0" :aria-valuemax="joueur[4].pvMax">
                                <i class="fas fa-heart icon-text"> : @{{ joueur[4].pv }}</i>
                            </div>   
                        </div>
                        <div class="progress md-progress">
                            <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[4].mana, joueur[4].manaMax)" :aria-valuenow="joueur[4].mana" aria-valuemin="0" :aria-valuemax="joueur[4].manaMax">
                                <i class="fas fa-fire-alt icon-text">  @{{ joueur[4].mana }}</i>
                            </div>   
                        </div>
                        <div class="row text-right">
                            <button 
                                v-if="capacite1J4.mana <= joueur[4].mana " 
                                type="button"
                                v-on:click="degat(capacite1J4, 4)" class="btn btn-success col-5"
                                >
                                    @{{capacite1J4.name}} <i class="fas fa-bomb"></i> @{{capacite1J4.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite1J4.mana}}
                            </button>
                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary"
                                >
                                    @{{capacite1J4.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite1J4.mana}}
                            </button>
                            <button 
                                v-if="capacite2J4.mana <= joueur[4].mana " 
                                type="button"
                                v-on:click="degat(capacite2J4, 4)" class="btn btn-success col-5"
                                >
                                    @{{capacite2J4.name}} <i class="fas fa-bomb"></i> @{{capacite2J4.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J4.mana}}
                            </button>
                            <button 
                                v-else 
                                type="button" 
                                class="btn btn-primary"
                                >
                                    @{{capacite2J4.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite2J4.mana}}
                            </button>
                            <span class="badge badge-danger text-right ml-2 justify-content-center" id="degatSpanJ4"></span>
                            <button 
                                v-if="capacite3J4.mana <= joueur[4].mana " 
                                type="button"
                                v-on:click="degat(capacite3J4, 4)" class="btn btn-success col-5"
                                >
                                    @{{capacite3J4.name}} <i class="fas fa-heart"></i> @{{capacite3J4.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J4.gainMana}}
                            </button>
                            <button 
                                v-else 
                                type="button"
                                class="btn btn-primary"
                                >
                                    @{{capacite3J4.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite3J4.mana}}
                            </button>
                            <button 
                                v-if="capacite4J4.mana <= joueur[4].mana " 
                                type="button"
                                v-on:click="degat(capacite4J4, 4)" class="btn btn-success col-5"
                                >
                                    @{{capacite4J4.name}} <i class="fas fa-heart"></i> @{{capacite4J4.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J4.mana}}
                            </button>
                            <button 
                                v-else 
                                type="button"
                                class="btn btn-primary"
                                >   
                                    @{{capacite4J4.name}}
                                    <i class="fas fa-fire-alt"></i> - @{{capacite4J4.mana}}
                            </button>
                        </div>                      
                    </div>
                </div>
            </div>

        <div class="row">
            <div class="col-sm-3">

            </div>

            <div class="col-sm-3 card center" id="joueur2">
                <div class="card-body text-center">
                    <h5 class="card-title">@{{ joueur[2].name }}</h5>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[2].pv, joueur[2].pvMax)" :aria-valuenow="joueur[2].pv" aria-valuemin="0" :aria-valuemax="joueur[2].pvMax">
                            <i class="fas fa-heart icon-text"> : @{{ joueur[2].pv }}</i>
                        </div>   
                    </div>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[2].mana, joueur[2].manaMax)" :aria-valuenow="joueur[2].mana" aria-valuemin="0" :aria-valuemax="joueur[2].manaMax">
                            <i class="fas fa-fire-alt icon-text"> : @{{ joueur[2].mana }}</i>
                        </div>   
                    </div>
                    <span class="badge badge-danger ml-2" id="degatSpanJ2"></span>
                    <div class="row">
                
                        <button 
                            v-if="capacite1J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite1J2, 2)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite1J2.name}}<i class="fas fa-bomb"></i> @{{capacite1J2.dmg}}<i class="fas fa-fire-alt"></i> - @{{capacite1J2.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite1J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite1J2.mana}}
                        </button>

                        <button 
                            v-if="capacite2J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite2J2, 2)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite2J2.name}} <i class="fas fa-bomb"></i> @{{capacite2J2.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J2.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite2J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite2J2.mana}}
                        </button>

                        <button 
                            v-if="capacite3J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite3J2, 2)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite3J2.name}} <i class="fas fa-heart"></i> @{{capacite3J2.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J2.gainMana}}
                        </button>
                        
                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite3J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite3J2.mana}}
                        </button>
                        
                        <button 
                            v-if="capacite4J2.mana <= joueur[2].mana" 
                            type="button"
                            v-on:click="degat(capacite4J2, 2)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite4J2.name}} <i class="fas fa-heart"></i> @{{capacite4J2.heal}}<i class="fas fa-fire-alt"></i> - @{{capacite4J2.mana}}
                        </button>
                        
                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite4J2.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite4J2.mana}}
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 card center" id="joueur3">
                <div class="card-body text-center">
                    <h5 class="card-title">@{{ joueur[3].name }}</h5>
                    <div class="progress md-progress">
                        <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[3].pv, joueur[3].pvMax)" :aria-valuenow="joueur[3].pv" aria-valuemin="0" :aria-valuemax="joueur[3].pvMax">
                            <i class="fas fa-heart icon-text"> : @{{ joueur[3].pv }}</i>
                        </div>   
                    </div>
                    <div class="progress md-progress">
                        <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[3].mana, joueur[3].manaMax)" :aria-valuenow="joueur[3].mana" aria-valuemin="0" :aria-valuemax="joueur[3].manaMax">
                            <i class="fas fa-fire-alt icon-text"> : @{{ joueur[3].mana }}</i> 
                        </div>   
                    </div>
                    <span class="badge badge-danger ml-2" id="degatSpanJ3"></span>
                    <div class="row">

                        <button 
                            v-if="capacite1J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite1J3, 3)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite1J3.name}} <i class="fas fa-bomb"></i> @{{capacite1J3.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite1J3.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite1J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite1J3.mana}}
                        </button>

                        <button 
                            v-if="capacite2J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite2J3, 3)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite2J3.name}} <i class="fas fa-bomb"></i> @{{capacite2J3.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J3.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite2J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite2J3.mana}}
                        </button>

                        <button 
                            v-if="capacite3J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite3J3, 3)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite3J3.name}} <i class="fas fa-heart"></i> @{{capacite3J3.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J3.gainMana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite3J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite3J3.mana}}
                        </button>

                        <button 
                            v-if="capacite4J3.mana <= joueur[3].mana "  
                            type="button"
                            v-on:click="degat(capacite4J3, 3)" 
                            class="btn btn-success col-5"
                            >
                                @{{capacite4J3.name}} <i class="fas fa-heart"></i> @{{capacite4J3.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J3.mana}}
                        </button>

                        <button 
                            v-else 
                            type="button" 
                            class="btn btn-primary"
                            >
                                @{{capacite4J3.name}}
                                <i class="fas fa-fire-alt"></i> - @{{capacite4J3.mana}}
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">

            </div>
        </div>    
    </section>
    <section id="testCarouselle">
        <div class="carousel-wrapper">
            <div class="carousel" data-flickity>
                <div class="carousel-cell">
                    <h3>Product 1</h3>
                    <a class="more" href="">Explore more</a>
                    <img src="https://images.unsplash.com/photo-1464305795204-6f5bbfc7fb81?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" />
                    <div class="line"></div>
                    <div class="price">
                    <span>225<sup>€</sup></span>
                </div>
                </div>
                <div class="carousel-cell">
                <h3>Product 2</h3>
                <a class="more" href="">Explore more</a>
                <img src="https://images.unsplash.com/photo-1464305795204-6f5bbfc7fb81?dpr=2&auto=format&fit=crop&w=1500&h=1000&q=80&cs=tinysrgb&crop=" />
                <div class="line"></div>
                <div class="price">
                    <span>225<sup>€</sup></span>
                </div>
                </div>
                <div class="carousel-cell">
                        <div class="card-body text-center">
                                <h5 class="card-title">@{{ joueur[3].name }}</h5>
                                <div class="progress md-progress">
                                    <div class="progress-bar bg-danger" role="progressbar" :style="styleWidthProgessBar(joueur[3].pv, joueur[3].pvMax)" :aria-valuenow="joueur[3].pv" aria-valuemin="0" :aria-valuemax="joueur[3].pvMax">
                                        <i class="fas fa-heart icon-text"> : @{{ joueur[3].pv }}</i>
                                    </div>   
                                </div>
                                <div class="progress md-progress">
                                    <div class="progress-bar" role="progressbar" :style="styleWidthProgessBar(joueur[3].mana, joueur[3].manaMax)" :aria-valuenow="joueur[3].mana" aria-valuemin="0" :aria-valuemax="joueur[3].manaMax">
                                        <i class="fas fa-fire-alt icon-text"> : @{{ joueur[3].mana }}</i> 
                                    </div>   
                                </div>
                                <span class="badge badge-danger ml-2" id="degatSpanJ3"></span>
                                <div class="row">
            
                                    <button 
                                        v-if="capacite1J3.mana <= joueur[3].mana "  
                                        type="button"
                                        v-on:click="degat(capacite1J3, 3)" 
                                        class="btn btn-success col-5"
                                        >
                                            @{{capacite1J3.name}} <i class="fas fa-bomb"></i> @{{capacite1J3.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite1J3.mana}}
                                    </button>
            
                                    <button 
                                        v-else 
                                        type="button" 
                                        class="btn btn-primary"
                                        >
                                            @{{capacite1J3.name}}
                                            <i class="fas fa-fire-alt"></i> - @{{capacite1J3.mana}}
                                    </button>
            
                                    <button 
                                        v-if="capacite2J3.mana <= joueur[3].mana "  
                                        type="button"
                                        v-on:click="degat(capacite2J3, 3)" 
                                        class="btn btn-success col-5"
                                        >
                                            @{{capacite2J3.name}} <i class="fas fa-bomb"></i> @{{capacite2J3.dmg}} <i class="fas fa-fire-alt"></i> - @{{capacite2J3.mana}}
                                    </button>
            
                                    <button 
                                        v-else 
                                        type="button" 
                                        class="btn btn-primary"
                                        >
                                            @{{capacite2J3.name}}
                                            <i class="fas fa-fire-alt"></i> - @{{capacite2J3.mana}}
                                    </button>
            
                                    <button 
                                        v-if="capacite3J3.mana <= joueur[3].mana "  
                                        type="button"
                                        v-on:click="degat(capacite3J3, 3)" 
                                        class="btn btn-success col-5"
                                        >
                                            @{{capacite3J3.name}} <i class="fas fa-heart"></i> @{{capacite3J3.heal}} <i class="fas fa-fire-alt"></i> + @{{capacite3J3.gainMana}}
                                    </button>
            
                                    <button 
                                        v-else 
                                        type="button" 
                                        class="btn btn-primary"
                                        >
                                            @{{capacite3J3.name}}
                                            <i class="fas fa-fire-alt"></i> - @{{capacite3J3.mana}}
                                    </button>
            
                                    <button 
                                        v-if="capacite4J3.mana <= joueur[3].mana "  
                                        type="button"
                                        v-on:click="degat(capacite4J3, 3)" 
                                        class="btn btn-success col-5"
                                        >
                                            @{{capacite4J3.name}} <i class="fas fa-heart"></i> @{{capacite4J3.heal}} <i class="fas fa-fire-alt"></i> - @{{capacite4J3.mana}}
                                    </button>
            
                                    <button 
                                        v-else 
                                        type="button" 
                                        class="btn btn-primary"
                                        >
                                            @{{capacite4J3.name}}
                                            <i class="fas fa-fire-alt"></i> - @{{capacite4J3.mana}}
                                    </button>
                                </div>
                            </div>
                </div>
                </div>
            </div>
            </div>
    </section>
    
    
</div>
@include('js')
@endsection
