[
    {
        name : 'soin1',
        description: "",
        heal: 5,
        dmg: 0,  
        gainMana : 0,         
        mana : 3,
        buff : "",
        combo : 0
                                                        
    },
    {
        name : 'soin2',
        description: "",
        heal: 7,
        dmg: 0,  
        gainMana : 0,         
        mana : 5,
        buff : "",
        combo : 0
                                                        
    },
    {
        name : 'soin3',
        description: "",
        heal: 9,
        dmg: 0,    
        gainMana : 0,       
        mana : 10,
        buff : "",
        combo : 0
                                                        
    },
    {
        name : 'soin4',
        description: "",
        heal: 11,
        dmg: 0, 
        gainMana : 0,          
        mana : 3,
        buff : "",
        combo : 0
                                                        
    },
],
//liste des capacité régenerante
manaSkill : [
    {
        name : 'regen',
        description: "",
        heal: 0,
        dmg: 0, 
        gainMana : 10,          
        mana : 0,
        buff : "",
        combo : 0
    },
    {
        name : 'regen 2',
        description: "",
        heal: 0,
        dmg: 0, 
        gainMana : 15,          
        mana : 0,
        buff : "",
        combo : 0
    },
    {
        name : 'regen 3',
        description: "",
        heal: -5,
        dmg: 0, 
        gainMana : 20,          
        mana : 0,
        buff : "",
        combo : 0
    },
    {
        name : 'regen 4',
        description: "",
        heal: -10,
        dmg: 0,
        gainMana : 25,
        mana : 0,
        buff : "",
        combo : 0
    },
]